import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import TakeAttnd from "../components/TakeAttnd";
import Students from "../components/Students";
import Lectures from "../components/Lectures";

Vue.use(VueRouter);

const routes = [{
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/lectures",
    name: "Lectures",
    component: Lectures
  },
  {
    path: "/attendance",
    name: "Attendance",
    component: TakeAttnd
  },
  {
    path: "/students",
    name: "Students",
    component: Students
  },
  {
    path: "/about",
    name: "About",

  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;